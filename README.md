# joincsv

This program will take a directory containing CSV files and output a single csv
file containing all the columns and data from the input CSVs.

This is a Rust learning project. I recently wrote an equivalent program in
Python.
