use csv::{Reader, Result, StringRecord, Writer};
use std::collections::HashMap;
use std::fs::{self, File};
use std::rc::Rc;

type Header = Rc<String>;
type Headers = Vec<Header>;
type Map = HashMap<Header, String>;
type Maps = Vec<Map>;

pub fn build_maps(input_dirname: &String) -> Result<Box<Maps>> {
    let entries = fs::read_dir(input_dirname)?;
    let mut maps: Box<Maps> = Box::new(Vec::new());

    // Loop through all entries, if they have a csv extension, add a mapping of header => value for
    // each record in the file. Potential bug: what if a directory ends with '.csv'?
    for entry in entries {
        let dir = entry?;

        if let Some(ext) = dir.path().extension() {
            if ext == "csv" {
                let mut reader = Reader::from_path(dir.path())?;
                let headers = get_file_headers(&mut reader)?;
                for result in reader.records() {
                    add_map(&mut maps, &headers, &result?);
                }
            }
        }
    }

    Ok(maps)
}

fn get_file_headers(reader: &mut Reader<File>) -> Result<Headers> {
    // Rc'ing headers so they can be shared between all rows in the file
    Ok(reader
        .headers()?
        .iter()
        .map(|s| Rc::new(s.to_string()))
        .collect())
}

fn add_map(maps: &mut Maps, headers: &Headers, record: &StringRecord) {
    let map: Map = headers
        .iter()
        .zip(record.iter())
        .map(|(k, v)| (Rc::clone(k), v.to_string()))
        .collect();
    maps.push(map);
}

pub fn write_maps(output_filename: &String, maps: &Maps) -> Result<()> {
    let headers = uniq_headers(&maps);
    let mut writer = Writer::from_path(output_filename)?;

    writer.write_record(headers.iter().map(|h| h.as_ref()).collect::<Vec<&String>>())?;

    let empty = String::from("");

    // Write out each record with all the headers. Some rows will not have all the headers, so we
    // write an empty string for those
    for map in maps.iter() {
        let record = headers.iter().map(|h| {
            if let Some(val) = map.get(h) {
                val
            } else {
                &empty
            }
        });
        writer.write_record(record)?;
    }

    Ok(())
}

fn uniq_headers(maps: &Maps) -> Box<Headers> {
    // Q: Is it necessary to box the vector in order to not do a big memory copy when returning it
    // from this function?
    let mut headers = Box::new(Vec::new());

    for map in maps.iter() {
        add_headers(&mut headers, map);
    }

    headers
}

fn add_headers(headers: &mut Headers, map: &Map) {
    for key in map.keys() {
        if !headers.contains(key) {
            headers.push(Rc::clone(key));
        }
    }
}
