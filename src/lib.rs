use csv::Result as CSVResult;
use std::env;

mod iter;
mod mem;

pub struct Config {
    pub input_dirname: String,
    pub output_filename: String,
}

impl Config {
    pub fn parse(mut args: env::Args) -> Result<Config, &'static str> {
        args.next();

        let input_dirname = match args.next() {
            Some(arg) => arg,
            None => return Err("Didn't get an input_dirname"),
        };

        let output_filename = match args.next() {
            Some(arg) => arg,
            None => return Err("Didn't get an output_filename"),
        };

        Ok(Config {
            input_dirname,
            output_filename,
        })
    }
}

pub fn process_in_memory(config: &Config) -> CSVResult<()> {
    let maps = mem::build_maps(&config.input_dirname)?;
    mem::write_maps(&config.output_filename, &maps)
}

pub fn process_iter(config: &Config) -> CSVResult<()> {
    let mut csvs = iter::get_csvs(&config.input_dirname)?;
    iter::write_csvs(&config.output_filename, &mut csvs)
}
