use csv::{Reader, Result, Writer};
use std::collections::HashMap;
use std::fs::{self, File};
use std::path::PathBuf;
use std::rc::Rc;

type Header = Rc<String>;
type Headers = Vec<Header>;
type Map = HashMap<Header, String>;

pub struct CSV {
    file_path: PathBuf,
    headers: Option<Headers>,
    reader: Option<Reader<File>>,
}

impl CSV {
    pub fn new(file_path: PathBuf) -> CSV {
        CSV {
            file_path,
            headers: None,
            reader: None,
        }
    }

    fn get_reader(&mut self) -> Result<&mut Reader<File>> {
        if let None = self.reader {
            self.reader = Some(Reader::from_path(&self.file_path)?);
        }
        Ok(self.reader.as_mut().unwrap())
    }

    pub fn get_headers(&mut self) -> Result<&Headers> {
        if let None = self.headers {
            self.headers = Some(
                self.get_reader()?
                    .headers()?
                    .iter()
                    .map(|s| Rc::new(s.to_string()))
                    .collect(),
            );
        }
        Ok(self.headers.as_ref().unwrap())
    }
}

impl Iterator for CSV {
    type Item = Result<Map>;

    fn next(&mut self) -> Option<Result<Map>> {
        match self.get_reader() {
            Ok(reader) => match reader.records().next() {
                Some(Ok(rec)) => match self.get_headers() {
                    Ok(headers) => {
                        let res: Map = headers
                            .iter()
                            .zip(rec.iter())
                            .map(|(k, v)| (Rc::clone(k), v.to_string()))
                            .collect();
                        Some(Ok(res))
                    }
                    Err(err) => Some(Err(err)),
                },
                Some(Err(err)) => Some(Err(err)),
                None => None,
            },
            Err(err) => Some(Err(err)),
        }
    }
}

pub fn get_csvs(input_dirname: &String) -> Result<Vec<CSV>> {
    let entries = fs::read_dir(input_dirname)?;
    let mut csvs = Vec::new();

    for entry in entries {
        let dir = entry?;

        if let Some(ext) = dir.path().extension() {
            if ext == "csv" {
                let csv = CSV::new(dir.path());
                csvs.push(csv);
            }
        }
    }

    Ok(csvs)
}

fn uniq_headers(csvs: &mut Vec<CSV>) -> Result<Headers> {
    let mut headers = Vec::new();

    for csv in csvs.iter_mut() {
        let hdrs = csv.get_headers()?;

        for hdr in hdrs {
            if !headers.contains(hdr) {
                headers.push(Rc::clone(hdr));
            }
        }
    }

    Ok(headers)
}

pub fn write_csvs(output_filename: &String, csvs: &mut Vec<CSV>) -> Result<()> {
    let headers = uniq_headers(csvs)?;
    let mut writer = Writer::from_path(output_filename)?;
    let filename = String::from("filename");
    let empty = String::from("");

    let mut header_record = Vec::new();
    for header in headers.iter() {
        header_record.push(header.as_ref());
    }
    header_record.push(&filename);
    writer.write_record(header_record)?;

    for csv in csvs.iter_mut() {
        let file_path = String::from(csv.file_path.to_str().unwrap());

        for res in csv {
            let rec = res?;
            let mut row = Vec::new();
            row.push(&file_path);
            for h in headers.iter() {
                if let Some(val) = rec.get(h) {
                    row.push(val);
                } else {
                    row.push(&empty);
                }
            }
            writer.write_record(row)?;
        }
    }

    Ok(())
}
