use joincsv::Config;
use std::env;
use std::process;

fn main() {
    let config = Config::parse(env::args()).unwrap_or_else(|err| {
        eprintln!("Problem parsing arguments: {}", err);
        process::exit(1);
    });

    // joincsv::process_in_memory(&config).unwrap();
    joincsv::process_iter(&config).unwrap();
}
